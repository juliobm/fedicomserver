package fedicom

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

func Licencia() {
	f, err := ioutil.ReadFile("./LICENCIA.txt")
	if err == nil {
		pinta := string(f)
		fmt.Println(pinta, "\n")
	}
}

func ErrLogfatal(err error, donde string) {
	if err != nil {
		fmt.Println("ErrLogfatal " + donde + "|")
		log.Panic(err)
	}
}

func ConfDefecto() {
	file, err := os.Open("./fedicomserver.ini")
	defer file.Close()
	if err == nil {
		var lines []string
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
		for _, line := range lines {
			var churro = strings.Split(line, "=")
			if (churro[0] != "") && (len(churro) > 1) {
				Conf[churro[0]] = churro[1]
			}
		}
	} else {
		Conf["escuchapuerto"] = "1111"
		Conf["debug"] = "1"
		Conf["reenviapedido"] = "0"
		Conf["consultasocio"] = "1"
		Conf["consultaexistencias"] = "1"
		Conf["creafichero"] = "1"
		Conf["rutafichero"] = "1"
		Conf["borrafaltas"] = "1"
		Conf["reenviaippedidos"] = "transfer.zacofarva.com"
		Conf["reenviapuerto"] = "1111"
		Conf["gestion"] = ""
		Conf["consultaip"] = "pedidos3.zacofarva.com"
		Conf["consultapuerto"] = "4444"
		Conf["ficherologs"] = "./logs/logsfedicom"
		Conf["randomfaltas"] = "0"
	}

	// selecting logFile
	var errl error
	t := time.Now()
	nano := t.Format("20060102")
	logFile, errl := os.Create(Conf["ficherologs"] + nano + ".txt")
	if errl != nil {
		// Open a different logfile or something
	}
	log.SetOutput(logFile)

}

func Fedlee(conn net.Conn) {
	// close connection on exit
	defer conn.Close()

	// we save what we received as it comes
	fo, err := os.Create("input.txt")
	ErrLogfatal(err, "en el create input")
	// close always
	defer func() {
		if err := fo.Close(); err != nil {
			panic(err)
		}
	}()

	// timeout at reading
	t := time.Now()
	conn.SetReadDeadline(t.Add(5 * time.Second))

	// valores
	fincadena := []byte("0199\r\n")
	buf := make([]byte, 0, 4096)
	tmp := make([]byte, 1024)
	formatocorrecto := 0

	// bucle hasta encontrar fincadena o timeout
	for {
		n, err := conn.Read(tmp)
		if err != nil {
			if err != io.EOF {
				fmt.Println("Fedlee read error:", err)
				log.Println("Fedlee read error:", err)
			}
			conn.Write([]byte("timeout"))
			break
		}
		buf = append(buf, tmp[:n]...)
		// escribimos en fichro
		if _, err := fo.Write(tmp[:n]); err != nil {
			fmt.Println("Fedlee error de escritura ", err)
			log.Println("Fedlee error de escritura ", err)
		}

		// comprobacion fin
		if bytes.Contains(buf, fincadena) {
			formatocorrecto = 1
			break
		}
	}
	respuesta := []byte("")
	if formatocorrecto == 1 {
		if Conf["reenviapedido"] == "2" {
			respuesta = []byte(Sendsocket(Conf["reenviaippedidos"], Conf["reenviapuerto"], string(buf[:]), fincadena))
		} else {
			respuesta = []byte(Fedpedido(buf))
			respuesta = append(respuesta, fincadena[:]...)
		}
		_, err2 := conn.Write(respuesta)
		if err2 != nil {
			conn.Close()
			return
		}
	} else {
		respuesta = append([]byte("no tiene formato correcto"), fincadena[:]...)
		_, err2 := conn.Write(respuesta)
		if err2 != nil {
			conn.Close()
			return
		}
	}
	conn.Close()
}
