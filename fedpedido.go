package fedicom

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
	"math/rand"
)

type Pedido struct {
	Fecha       string
	Hora        string
	Usuario     string
	Pass        string
	Gestion     string
	Cliente     string
	Numpedido   string
	Tipopedido  string
	Condserv    string
	Cargocoop   string
	Aplazacargo string
	Aplazapago  string
	Dtoped      string
	Empfac      string
	Almser      string
	Fecenvio    string
	Diaenvio    string

	Totallineas   int
	Totalcantidad int

	codcant map[string]string
}

var Conf = make(map[string]string)

func Fedpedido(buf []byte) string {

	pedidocorrecto := "2011Pedido recibido\n"
	pedidostatus := pedidocorrecto
	hazpedido := "si"
	inisesion := "0101"
	inipedcab := "1010"
	var miped Pedido
	miped.codcant = make(map[string]string)

	lineas := strings.Replace(string(buf[:]), "\r", "", -1)
	regs := strings.SplitAfter(lineas, "\n")

	if len(regs[0]) < 42 {
		if Conf["debug"] > "0" {
			fmt.Println("formato registro 0101 incorrecto")
			log.Println("formato registro 0101 incorrecto")
		}
		return "9999Formato inicio 0101 incorrecto\n"
	}

	if len(regs[1]) < 70 {
		if Conf["debug"] > "0" {
			fmt.Println("formato registro 1010 incorrecto")
			log.Println("formato registro 1010 incorrecto")
		}
		return "9999Formato cabecera 1010 incorrecto\n"
	}

	if regs[0][0:4] != inisesion {
		return "9999inicio incorrecto\n"
	}
	if regs[1][0:4] != inipedcab {
		return "9999cabecera pedido incorrecto\n"
	}
	miped.Fecha = regs[0][4:12]
	miped.Hora = regs[0][12:18]
	miped.Usuario = regs[0][18:34]
	miped.Pass = regs[0][34:42]
	miped.Gestion = "Pedido"
	if len(strings.Split(miped.Usuario, "CC")) > 1 {
		miped.Gestion = "Compromiso"
	}
	if len(strings.Split(miped.Usuario, "TR")) > 1 {
		miped.Gestion = "Transfer"
	}

	miped.Cliente = regs[1][4:20]
	miped.Numpedido = regs[1][20:30]
	miped.Tipopedido = regs[1][30:36]
	miped.Condserv = regs[1][36:42]
	miped.Cargocoop = regs[1][42:43]
	miped.Aplazacargo = regs[1][43:46]
	miped.Aplazapago = regs[1][46:49]
	miped.Dtoped = regs[1][49:53]
	miped.Empfac = regs[1][53:56]
	miped.Almser = regs[1][56:60]
	miped.Fecenvio = regs[1][60:68]
	miped.Diaenvio = regs[1][68:70]

	if Conf["debug"] > "0" {
		fmt.Println(miped.Gestion, " recibido del socio ", miped.Usuario, "/", miped.Cliente)
		log.Println(miped.Gestion, " recibido del socio ", miped.Usuario, "/", miped.Cliente)
	}

	if miped.Gestion == "Compromiso" {
		_, err := time.Parse("2006-01-02", miped.Fecenvio[0:4]+"-"+miped.Fecenvio[4:6]+"-"+miped.Fecenvio[6:8])
		if err != nil {
			if Conf["debug"] > "0" {
				fmt.Println("Compromiso con fecha fin no correcta: ", miped.Fecenvio)
				log.Println("Compromiso con fecha fin no correcta: ", miped.Fecenvio)
				return "2010" + miped.Cliente + miped.Numpedido + "1" + "00000000000FECHA COMPROMISO no admitida  \n"
			}
		}
	}

	// we check customer ?
	if Conf["consultasocio"] == "1" {
		existesocio := ExisteSocio(string(miped.Cliente))
		if existesocio != "1" {
			return "9999socio no existe\n"
		}
	}

	// cogemos las lineas del pedido
	for i := 2; i < len(regs)-1; i++ {
		fuemal := Fedlineas(regs[i], &miped)
		if fuemal != "" {
			pedidostatus = fuemal
			if Conf["debug"] > "0" {
				fmt.Println("error en las lineas del pedido ", fuemal)
				log.Println("error en las lineas del pedido ", fuemal)
			}
			break
		}
	}

	if len(miped.codcant) < 1 {
		if Conf["debug"] > "0" {
			fmt.Println("formato lineas incorrecto no hay")
			log.Println("formato lineas incorrecto no hay")
		}
		return "9999Formato lineas incorrecto\n"
	}

	// pedido todo correcto guardamos o/y enviamos
	if pedidostatus == pedidocorrecto {
		// faults
		if (Conf["consultaexistencias"] == "1") && (miped.Gestion == "Pedido") ||  Conf["randomfaltas"] > "0" {
			pedidostatus, hazpedido = Fedfaltas(&miped)
		}
		// transfer y compromisos compra
		if miped.Gestion == "Transfer" || miped.Gestion == "Compromiso" {
			pedidostatus, hazpedido = Fedexisteart(&miped)
		}
		// generate order
		if hazpedido == "si" {
			if Conf["creafichero"] == "1" {
				Fedgraba(&miped)
			}
			if Conf["reenviapedido"] == "1" {
				FedResend(buf, &miped)
				//FedResendPython(fic)
			}
		}
	}

	if Conf["debug"] > "0" {
		fmt.Println("quiero mas pedidos......")
		log.Println("quiero mas pedidos......")
	}
	return pedidostatus

}

func Fedlineas(r string, p *Pedido) string {
	error := ""

	inilin := "1020"
	inilinbon := "1030"
	inicheck := "1050"
	if len(r) < 4 {
		return error
	}
	los4 := r[0:4]

	switch los4 {
	case inilin:
		if len(r) < 21 {
			if Conf["debug"] > "0" {
				fmt.Println("formato registro 1010 incorrecto")
				log.Println("formato registro 1010 incorrecto")
			}
			error = "9999Formato linea 1020 incorrecto"
		}

		codigo := string(r[4:17])
		cantidad := string(r[17:21])
		if cantidad, err := strconv.Atoi(cantidad); err == nil {
			p.codcant[codigo] = strconv.Itoa(cantidad)
			p.Totallineas++
			p.Totalcantidad += cantidad

		}
	case inilinbon:
		if len(r) < 29 {
			if Conf["debug"] > "0" {
				fmt.Println("formato registro 1010 incorrecto")
				log.Println("formato registro 1010 incorrecto")
			}
			error = "9999Formato linea 1030 incorrecto"
		}

		codigo := string(r[4:17])
		cantidad := string(r[17:21])
		bonif := string(r[21:25])
		dtolin := string(r[25:27])
		dtolindec := string(r[27:29])
		if cantidad, err := strconv.Atoi(cantidad); err == nil {
			bonifnum, _ := strconv.Atoi(bonif)
			dtolinnum, _ := strconv.Atoi(dtolin)
			dtolindecnum, _ := strconv.Atoi(dtolindec)
			p.codcant[codigo] = strconv.Itoa(cantidad) + ";" +
				strconv.Itoa(bonifnum) + ";" +
				strconv.Itoa(dtolinnum) + "." +
				strconv.Itoa(dtolindecnum)
			p.Totallineas++
			p.Totalcantidad += cantidad
		}
	case inicheck:
		if len(r) < 20 {
			if Conf["debug"] > "0" {
				fmt.Println("formato registro 1010 incorrecto")
				log.Println("formato registro 1010 incorrecto")
			}
			error = "9999Formato linea 1050 incorrecto"
		}

		totlin := string(r[4:8])
		totcant := string(r[8:14])
		//		totbon := r[14:20]
		if totlin, err := strconv.Atoi(totlin); err == nil {
			if totcant, err2 := strconv.Atoi(totcant); err2 == nil {
				if (totlin != p.Totallineas) || (totcant != p.Totalcantidad) {
					fmt.Println("no cuadra check")
					log.Println("no cuadra check")
					error = "9999error checksum"
				}

			} else {
				error = "error en convert totcant"
			}

		} else {
			error = "error en convert totlin"
		}
	default:
	}

	return error

}

func Fedfaltas(p *Pedido) (string, string) {
	if Conf["debug"] > "1" {
		fmt.Println("consulta existencias para faltas")
		log.Println("consulta existencias para faltas")
	}
	inifal := "2015"
	pedconfaltas := "2011Pedido con incidencias\n"
	pedsinfaltas := "2011Pedido recibido\n"
	codinc := "02"
	bonstr := "00000000"
	codsus := "0000000000000\n"
	faltas := string("")
	pidezero := "0000"
	pidefmt := ""
	pidedesde := 0
	codigosenfalta := 0
	hazpedido := "si"

	// socket petition real stock
	churro, churro6, codigo, codigo6, devol := "", "", "", "", ""
	//churropre := "ExistenciasReales#" + string(p.Cliente) + "$"
	churropre := "ExistenciasReales#"
	churropos := "\r\n\r\n"
	for key, _ := range p.codcant {
		codigo, codigo6 = string(key), string(key)
		churro = churro + codigo + "|"
		if len(codigo6) > 6 {
			codigo6 = codigo[len(codigo)-7 : len(codigo)-1]
			if churro6 == "" {
				churro6 = churro6 + codigo6
			} else {
				churro6 = churro6 + "|" + codigo6
			}
		}
		if (Conf["randomfaltas"] > "0") {
			randomfaltas, _ := strconv.Atoi(Conf["randomfaltas"])
			falta := rand.Intn(randomfaltas)
			if (falta == 1) {
				devol = devol + "0|"
			} else {
				devol = devol + "9999|"
			}
			if Conf["debug"] > "1" {
			  fmt.Println("randomfaltas:", falta, "devol:", devol)
			}
		}
	}
	churro = churro[:len(churro)-1]

	if (Conf["randomfaltas"] == "0") {
		devol = Sendsocket(Conf["consultaip"], Conf["consultapuerto"], churropre+churro6+churropos, make([]byte, 1))
	}

	devol = string(devol[:])
	// only generate order without stock faults
	for key, codigo := range strings.Split(churro, "|") {
		pidestr := strings.Split(p.codcant[codigo], ";")[0]
		haystr := strings.Split(devol, "|")[key]
		hayint, _ := strconv.Atoi(haystr)
		pideint, _ := strconv.Atoi(pidestr)
		if Conf["debug"] > "1" {
			fmt.Println("pidestr:", pidestr, "haystr:", haystr)
			log.Println("pidestr:", pidestr, "haystr:", haystr)
		}
		if pideint > hayint {
			codigosenfalta++
			if Conf["borrafaltas"] == "1" {
				delete(p.codcant, codigo)
			}
			//p.Totallineas--
			//p.Totalcantidad -= pideint

			pidedesde = len(pidezero) - len(pidestr)
			pidefmt = pidezero[:pidedesde] + pidestr
			faltas = faltas + inifal + codigo + pidefmt + pidefmt + bonstr + codinc + codsus
		}
	}

	// informing about faults
	if faltas != "" {
		faltas = pedconfaltas + faltas
	} else {
		faltas = pedsinfaltas
	}

	// is there any article to serve?
	if len(p.codcant) < 1 {
		hazpedido = "no"
	}

	return faltas, hazpedido
}

func Fedexisteart(p *Pedido) (string, string) {
	if Conf["debug"] > "1" {
		fmt.Println("consulta existe articulo")
		log.Println("consulta existe articulo")
	}
	inifal := "2015"
	pedconfaltas := "2011" + p.Gestion + " con incidencias\n"
	pedsinfaltas := "2011" + p.Gestion + " recibido\n"
	codinc := "04"
	bonstr := "00000000"
	codsus := "0000000000000\n"
	faltas := string("")
	pidezero := "0000"
	pidefmt := ""
	pidedesde := 0
	codigosenfalta := 0
	hazpedido := "si"

	// socket petition existen articulos
	churro, churro6, codigo, codigo6 := "", "", "", ""
	churropre := "ExisteArticulo#"
	churropos := "\r\n\r\n"
	for key, _ := range p.codcant {
		codigo, codigo6 = string(key), string(key)
		churro = churro + codigo + "|"
		if len(codigo6) > 6 {
			codigo6 = codigo[len(codigo)-7 : len(codigo)-1]
			if churro6 == "" {
				churro6 = churro6 + codigo6
			} else {
				churro6 = churro6 + "|" + codigo6
			}
		}
	}
	churro = churro[:len(churro)-1]
	devol := Sendsocket(Conf["consultaip"], Conf["consultapuerto"], churropre+churro6+churropos, make([]byte, 1))
	devol = string(devol[:])
	// only generate order without stock faults
	for key, codigo := range strings.Split(churro, "|") {
		pidestr := strings.Split(p.codcant[codigo], ";")[0]
		haystr := strings.Split(devol, "|")[key]
		hayint, _ := strconv.Atoi(haystr)
		if Conf["debug"] > "1" {
			fmt.Println("existecodigo:", codigo, "haystr:", haystr)
			log.Println("existecodigo:", codigo, "haystr:", haystr)
		}
		if hayint == 0 {
			codigosenfalta++

			pidedesde = len(pidezero) - len(pidestr)
			pidefmt = pidezero[:pidedesde] + pidestr
			faltas = faltas + inifal + codigo + pidefmt + pidefmt + bonstr + codinc + codsus
		}
	}

	// informing about faults
	if faltas != "" {
		faltas = pedconfaltas + faltas
	} else {
		faltas = pedsinfaltas
	}

	// is there any article to serve?
	if len(p.codcant) < 1 {
		hazpedido = "no"
	}

	return faltas, hazpedido
}

func ExisteSocio(socio string) string {
	// comprobamos que exista el cliente
	if Conf["debug"] > "1" {
		fmt.Println("consulta existe socio ", socio)
		log.Println("consulta existe socio ", socio)
	}
	churrosocio := "ExisteSocio#" + socio + "\r\n\r\n"
	churrosocio = strings.Replace(churrosocio, " ", "", -1)
	existesocio := Sendsocket(Conf["consultaip"], Conf["consultapuerto"], churrosocio, make([]byte, 1))
	if len(existesocio) > 1 {
		existesocio = existesocio[0:1]
	}
	return existesocio
}

func Sendsocket(ip, port, manda string, finbyte []byte) string {
	// connect
	if ip == "" {
		errsock := "sendsocket no se permite ip nula"
		fmt.Println(errsock)
		return errsock
	}

	if Conf["debug"] > "1" {
		fmt.Println("consultaip:", ip, "consultaport:", port, "finbyte:", string(finbyte[:]))
		log.Println("consultaip:", ip, "consultaport:", port, "finbyte:", string(finbyte[:]))
	}
	host := ip + ":" + port
	conn, err := net.Dial("tcp", host)
	ErrLogfatal(err, "en el send "+manda)
	defer conn.Close()

	// timeout at reading
	t := time.Now()
	conn.SetReadDeadline(t.Add(5 * time.Second))

	// write to socket
	conn.Write([]byte(manda))

	// read from socket
	reply := make([]byte, 0, 1024)
	tmp := make([]byte, 1024)

	// bucle hasta encontrar fincadena o timeout
	for {
		n, err := conn.Read(tmp)
		if err != nil {
			if err != io.EOF {
				fmt.Println("Fedlee read error:", err)
				log.Println("Fedlee read error:", err)
			}
			conn.Write([]byte("timeout"))
			break
		}
		reply = append(reply, tmp[:n]...)

		// comprobacion fin
		if bytes.Contains(reply, finbyte) {
			break
		}
	}

	if Conf["debug"] > "1" {
		fmt.Println("reply:", string(reply[:]))
		log.Println("reply:", string(reply[:]))
	}
	return string(reply[:])
}

func Fedgraba(p *Pedido) string {
	// open text file to save order
	t := time.Now().Unix()
	nano := strconv.FormatInt(t, 10)
	nano = nano[len(nano)-5:]
	fic := Conf["rutafichero"] + p.Fecha + "_" + p.Hora + "_" + p.Usuario + "_" + p.Cliente + "_"
	fic = fic + nano + ".txt"
	fic = strings.Replace(fic, ":", "", -1)
	fic = strings.Replace(fic, " ", "", -1)
	if Conf["debug"] > "0" {
		fmt.Println("creando fichero ", fic)
		log.Println("creando fichero ", fic)
	}

	file, err := os.Create(fic)
	ErrLogfatal(err, "en el graba"+fic)

	defer file.Close()
	churro := "[principal]" +
		"\npuertoorigen=" + Conf["escuchapuerto"] +
		"\ndireccion=" + Conf["reenviaippedidos"] + "\npuerto=" + Conf["reenviapuerto"] +
		"\nusuario=" + p.Usuario + "\ncontra=" + p.Pass + "\ncliente=" + p.Cliente +
		"\ntpedido=" + p.Tipopedido + "\npedido=" + p.Numpedido + "\ndtopedido=" + p.Dtoped[0:2] +
		"." + p.Dtoped[2:] + "\naplazamiento=" + p.Aplazapago + "\nfechaenvio=" + p.Fecenvio +
		"\nalmser=" + p.Almser +
		"\n\n[articulos]\n"

	for key, value := range p.codcant {
		churro = churro + string(key) + "=" + value + "\n"
	}

	_, err = file.Write([]byte(churro))
	ErrLogfatal(err, "en el graba churro")

	file.Close()
	return fic
}

func FedResend(newbuf []byte, p *Pedido) {
	if Conf["debug"] > "0" {
		fmt.Println("reenviando pedido a ", Conf["reenviaippedidos"], Conf["reenviapuerto"])
		log.Println("reenviando pedido a ", Conf["reenviaippedidos"], Conf["reenviapuerto"])
	}
	newrespuesta := Sendsocket(Conf["reenviaippedidos"], Conf["reenviapuerto"], string(newbuf[:]), []byte("\n0199\r\n"))
	if Conf["debug"] > "1" {
		fmt.Println("newr:", newrespuesta)
		log.Println("newr:", newrespuesta)
	}

	// save the response in a text file
	t := time.Now().Unix()
	nano := strconv.FormatInt(t, 10)
	nano = nano[len(nano)-5:]
	fic := Conf["rutafichero"] + p.Fecha + "_" + p.Hora + "_" + p.Usuario + "_" + p.Cliente + "_"
	fic = fic + nano + "_respuesta.txt"
	fic = strings.Replace(fic, ":", "", -1)
	fic = strings.Replace(fic, " ", "", -1)
	newfile, err := os.Create(fic)
	ErrLogfatal(err, "en el graba"+fic)
	defer newfile.Close()
	_, err = newfile.Write([]byte(newrespuesta))
	ErrLogfatal(err, "reenviando el pedido")
	newfile.Close()
}
