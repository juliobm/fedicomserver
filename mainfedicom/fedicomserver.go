package main

import (
//	"io"
//	"io/ioutil"
	"fmt"
	"log"
	"net"
//	"os"
//	"time"
//	"bytes"
	//"strconv"
	"fedicom"
)

func main() {
    // licencia y autor
    fedicom.Licencia()

    // load configuration ini
	fedicom.ConfDefecto()

	// Listen on TCP port 1111 on all interfaces.
	if fedicom.Conf["debug"] > "0" { 
        fmt.Println("a la espera del primer pedido...") 
        log.Println("a la espera del primer pedido...") 
    }
	l, err := net.Listen("tcp", ":" + fedicom.Conf["escuchapuerto"])
	fedicom.ErrLogfatal(err, "en el listen del puerto")
	defer l.Close()
	for {
		// Wait for a connection.
		conn, err := l.Accept()
		fedicom.ErrLogfatal(err, "en el accept")
		// Handle the connection in a new goroutine.
		// The loop then returns to accepting, so that
		// multiple connections may be served concurrently.

		go fedicom.Fedlee(conn)
	}
}


