package fedicom

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"os/exec"
	"time"
)

func FedResendPython(fic string)  {	
	// now, do you want to send the file to another server?
	if Conf["debug"] > "0" { fmt.Println("reenviando el fichero generado") }
	cmd := exec.Command("/usr/bin/python", "/home/me/work/src/fedicom/fedicom_fileini.py", fic)
	err := cmd.Start()
	ErrLogfatal(err, "en el start python")
}



func Sendsocket2(ip, port, manda string, finbyte []byte) string {
    	// connect
	if ip == "" { 
		errsock := "sendsocket no se permite ip nula"
		fmt.Println(errsock)
		return errsock
	}
	
	if Conf["debug"] > "1" {fmt.Println("finbyte:", finbyte[:])}
	host := ip + ":" + port
    	conn, err := net.Dial("tcp", host)
    	ErrLogfatal(err, "en el send " + manda)
    	defer conn.Close()

	// timeout at reading
	t := time.Now()
	conn.SetReadDeadline(t.Add(5 * time.Second))	

    	// write to socket
    	conn.Write([]byte(manda))

    	// read from socket
    	reply := make([]byte, 1024)
	n, err := conn.Read(reply)
	if err != nil {
		if err != io.EOF {
			fmt.Println("sendsocket read error:", err)
		}	
		conn.Write([]byte("timeout"))
	}
		
	if Conf["debug"] > "1" {fmt.Println("reply:", reply[:])}
	n = bytes.IndexByte(reply, 0)
	return string(reply[:n])
}

